﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            ushort[] FREQUENCY = {392,392,440,392,523,494,
                392,392,440,392,587,523,
                392,392,784,659,523,494,440,
                689,689,659,523,587,523};

            ushort[] DELAY = {375,125,500,500,500,1000,
                375,125,500,500,500,1000,
                375,125,500,500,500,500,500,
                375,125,500,500,500,1000};
            int i;
            for (i = 0; i < 25; i++)
            {
                Console.Beep(FREQUENCY[i], DELAY[i]);
            }
        }
    }
}
